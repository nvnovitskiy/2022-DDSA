from random import randint

from loguru import logger
from tqdm import tqdm

from ChordNode import ChordNode

NUMBER_NODE = 14  # Количество узлов
BYTE_LENGTH = 7  # Количество бит в хеш-ключе


def stabilization(nodes):
    logger.info('Начат процесс по стабилизации узлов...')

    for _ in tqdm(range(len(nodes) * len(nodes) + 1)):
        for node in nodes:
            node.stabilize()
            node.fix_fingers()

    logger.info('Процесс стабилизации узлов был закончен...')

    for node in nodes:
        logger.debug(f'{str(node)}')


if __name__ == '__main__':
    node_list = []
    main_node = ChordNode(0, BYTE_LENGTH)
    main_node.join(None)
    node_list.append(main_node)

    for n in range(1, NUMBER_NODE):
        chord_node = ChordNode(n, BYTE_LENGTH)
        chord_node.join_to_node(main_node)
        node_list.append(chord_node)

    logger.info('Узлы созданы...')
    stabilization(node_list)

    logger.info('Добавляем узлы...')
    new_node_1 = ChordNode(125, BYTE_LENGTH)
    new_node_1.join_to_node(main_node)
    node_list.append(new_node_1)
    
    new_node_2 = ChordNode(83, BYTE_LENGTH)
    new_node_2.join_to_node(main_node)
    node_list.append(new_node_2)
    
    new_node_3 = ChordNode(36, BYTE_LENGTH)
    new_node_3.join_to_node(main_node)
    node_list.append(new_node_3)
    
    new_node_4 = ChordNode(55, BYTE_LENGTH)
    new_node_4.join_to_node(main_node)
    node_list.append(new_node_4)
   
    new_node_5 = ChordNode(22, BYTE_LENGTH)
    new_node_5.join_to_node(main_node)
    node_list.append(new_node_5)
    
    new_node_6 = ChordNode(96, BYTE_LENGTH)
    new_node_6.join_to_node(main_node)
    node_list.append(new_node_6)

    stabilization(node_list)
    logger.info('Узлы добавлены...')

    logger.info('Удаляем добавленные узлы...')
    node_list.remove(new_node_1)
    new_node_1.remove()

    node_list.remove(new_node_3)
    new_node_3.remove()

    node_list.remove(new_node_5)
    new_node_5.remove()
    stabilization(node_list)
    logger.info('Узлы удалены успешно...')