from typing import Tuple


def generate_start(m: int, n: int, i: int) -> int:
    """Функция, которая генерирует старотовое значение Finger."""
    return (n + 2 ** i) % 2 ** m


class Finger:
    """"Класс для хранения вспомогательной информации об узлах."""

    interval: Tuple[int, int]
    node = None

    def __init__(self, n: int, m: int, i: int, node):
        """Инициализациия ифнормации об узлах.
        :param n: количество узлов
        :param m: количество бит, используемых для генерации идентификаторов
        :param i: индекс входа
        :param node: узел
        """
        self.start = generate_start(n, m, i)
        self.end = generate_start(n, m, i + 1)
        self.interval = (self.start, self.end)
        self.node = node