# Моделирование алгоритма Chord

Теория: [Wikipedia](https://en.wikipedia.org/wiki/Chord_(peer-to-peer))

Запуск программы: ```python main.py```

Результат работы программы: 

![result_image](https://github.com/nvnovitskiy/2022-DDSA/blob/main/Modeling%20of%20the%20Chord%20algorithm/images/img.png)
